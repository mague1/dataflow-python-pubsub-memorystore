# Copyright 2019 Google LLC.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# [START pubsub_to_memorystore]
import argparse
import datetime
import logging
from random import randint
import sys
import time
import json

from redis import Redis
from redis.cluster import RedisCluster as Redis

from apache_beam import (
    DoFn,
    GroupByKey,
    io,
    ParDo,
    Pipeline,
    PTransform,
    WindowInto,
    WithKeys,
)
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.transforms.window import FixedWindows


class GroupMessagesByFixedWindows(PTransform):
    """A composite transform that groups Pub/Sub messages based on publish time
    and outputs a list of tuples, each containing a message and its publish time.
    """

    def __init__(self, window_size, num_shards=5):
        # Set window size to 60 seconds.
        self.window_size = int(window_size)
        self.num_shards = num_shards

    def expand(self, pcoll):
        return (
            pcoll
            # Bind window info to each element using element timestamp (or publish time).
            | "Window into fixed intervals"
            >> WindowInto(FixedWindows(self.window_size))
            # No longer necessary #| "Add timestamp to windowed elements" >> ParDo(AddTimestamp())
            # Assign a random key to each windowed element based on the number of shards.
            | "Add key" >> WithKeys(lambda _: randint(0, self.num_shards - 1))
            # Group windowed elements by key. All the elements in the same window must fit
            # memory for this. If not, you need to use `beam.util.BatchElements`.
            | "Group by key" >> GroupByKey()
        )


class WriteToMemorystore(DoFn):
    def __init__(self, redis_host, redis_port, redis_password, redis_key):
        self.redis_host = redis_host
        self.redis_port = redis_port
        self.redis_password = redis_password
        self.redis_key = redis_key
        # if you try to initialize the redis cluster here life will be bad

    def process(self, key_value, window=DoFn.WindowParam):
        """Write messages in a batch to Google Cloud Memorystore."""

        ts_format = "%H:%M"
        print(window)
        shard_id, batch = key_value
        print("shard_id: " + str(shard_id))
        print("batch: " + str(batch))

        rc = Redis(
            host=self.redis_host, port=self.redis_port, password=self.redis_password
        )
        ##for message_body, publish_time in batch:
        for message_body in batch:
            try:
                js = json.loads(message_body.decode("utf-8"))
                rc.hset(js[self.redis_key], mapping=js)
            except:
                print("Could not decode:")
                print(f"{message_body}")

        rc.close()


def run(
    input_topic,
    window_size=1.0,
    num_shards=5,
    redis_host="localhost",
    redis_port=6379,
    redis_password=None,
    redis_key=None,
    pipeline_args=None,
):
    # Set `save_main_session` to True so DoFns can access globally imported modules.
    pipeline_options = PipelineOptions(
        pipeline_args, streaming=True, save_main_session=True
    )

    with Pipeline(options=pipeline_options) as pipeline:
        (
            pipeline
            | "Read from Pub/Sub" >> io.ReadFromPubSub(topic=input_topic)
            | "Window into" >> GroupMessagesByFixedWindows(window_size, num_shards)
            | "Write to Memorystore" >> ParDo(WriteToMemorystore(redis_host, redis_port, redis_password, redis_key))
        )


if __name__ == "__main__":
    logging.getLogger().setLevel(logging.INFO)

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--input_topic",
        help="The Cloud Pub/Sub topic to read from."
        '"projects/<PROJECT_ID>/topics/<TOPIC_ID>".',
    )
    parser.add_argument(
        "--window_size",
        type=float,
        default=1.0,
        help="Output file's window size in minutes.",
    )
    parser.add_argument(
        "--redis_host",
        default="localhost",
        required=True,
        help="Redis Endpoint host",
    )
    parser.add_argument(
        "--redis_port",
        type=int,
        default=6379,
        help="Redis Endpoint port",
    )
    parser.add_argument(
        "--redis_password",
        default=None,
        help="Redis Endpoint Password",
    )
    parser.add_argument(
        "--redis_key",
        default=None,
        required=True,
        help="Redis Endpoint Password",
    )
    parser.add_argument(
        "--num_shards",
        type=int,
        default=5,
        help="Number of shards to use when writing windowed elements to Memorystore.",
    )
    known_args, pipeline_args = parser.parse_known_args()

    run(
        known_args.input_topic,
        known_args.window_size,
        known_args.num_shards,
        known_args.redis_host,
        known_args.redis_port,
        known_args.redis_password,
        known_args.redis_key,
        pipeline_args,
    )
# [END pubsub_to_memorystore]
