# Write PubSub to Memorystore using Dataflow

![diagram](./docs/Dataflow-Python-Pubsub-Memorystore.png)

## Environment Setup

```shell
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```


## Running the Dataflow Job

### Enable all of the gcloud APIs

```shell
gcloud services enable dataflow.googleapis.com  compute.googleapis.com \
  logging.googleapis.com  storage-component.googleapis.com  storage-api.googleapis.com \
  pubsub.googleapis.com  cloudresourcemanager.googleapis.com  cloudscheduler.googleapis.com
```

### Set Environment Variables

```shell
export TOPIC_ID=maguec-dataflow-topic
export BUCKET_ID=maguec-dataflow-gcs
export PROJECT_ID=$(gcloud config get-value project)
gcloud pubsub topics create $TOPIC_ID
gsutil mb gs://$BUCKET_ID
```

### Firewall setup

Ensure the network that the runner is on has access to ports 

#### Source filters
  IP ranges 10.0.0.0/8
  
#### Protocols and ports
  tcp:6379
  tcp:6380-64000

### Actual runner

```shell
python PubSubToMemorystore.py --project=${PROJECT_ID} --region=us-central1 \
    --input_topic=projects/${PROJECT_ID}/topics/${TOPIC_ID} --redis_host=8.8.8.8 \
    --window_size=1 --redis_key=user --network=projects/${PROJECT_ID}/networks/default \
    --runner=DataflowRunner  --temp_location=gs://${BUCKET_ID}/temp
```
Note: replace the 8.8.8.8 with the IP address of your secondary Memorystore Cluster

### Send some fake data

```shell
./util/pubexample.py --redis_host 192.168.0.228 --redis_port 30001 --pubsub_project $PROJECT_ID --pubsub_topic $TOPIC_ID
```
Replace the --redis_host and --redis_port with your local Memorystore
