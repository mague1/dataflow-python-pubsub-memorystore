#!/usr/bin/env python

import warnings
warnings.filterwarnings("ignore")


import argparse
import random
import json
from redis import Redis
from redis.cluster import RedisCluster as Redis
from faker import Faker

from concurrent import futures
from google.cloud import pubsub_v1
from typing import Callable

def get_callback(
    publish_future: pubsub_v1.publisher.futures.Future, data: str
) -> Callable[[pubsub_v1.publisher.futures.Future], None]:
    def callback(publish_future: pubsub_v1.publisher.futures.Future) -> None:
        try:
            # Wait 60 seconds for the publish call to succeed.
            print(publish_future.result(timeout=60))
        except futures.TimeoutError:
            print(f"Publishing {data} timed out.")

    return callback


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--pubsub_project",
        required=True,
        help="GCP Project Name",
    )
    parser.add_argument(
        "--pubsub_topic",
        required=True,
        help="GCP Pubsub Topic",
    )
    parser.add_argument(
        "--redis_host",
        default="localhost",
        required=True,
        help="Redis Endpoint host",
    )
    parser.add_argument(
        "--redis_port",
        type=int,
        default=6379,
        help="Redis Endpoint port",
    )
    parser.add_argument(
        "--redis_password",
        default=None,
        help="Redis Endpoint Password",
    )
    parser.add_argument(
        "--user_count",
        type=int,
        default=100,
        help="Number of fake users to add",
    )

    known_args, pipeline_args = parser.parse_known_args()

    fake = Faker()

    rc = Redis(
        host=known_args.redis_host, port=known_args.redis_port, password=known_args.redis_password
    )


    for u in range(known_args.user_count):
        publisher = pubsub_v1.PublisherClient()
        topic_path = publisher.topic_path(known_args.pubsub_project, known_args.pubsub_topic)
        publish_futures = []
        fn = fake.first_name()
        ln = fake.last_name()
        user = {
            "user": "{}{}".format(fake.user_name(), random.randint(1950, 2001)),
            "firstname": fn,
            "lastname": ln,
            "email": "{}.{}@{}.com".format(
                fn,
                ln,
                fake.safe_color_name(),
            ),
            "email2": fake.ascii_free_email(),
            "cell": fake.msisdn(),
            "ssn": fake.ssn(),
            "bank": fake.credit_card_provider(),
            "cardnumber": fake.credit_card_number(),
            "expire": fake.credit_card_expire(),
            "rating": random.randint(1, 100),
        }
        data = json.dumps(user)
        print("Published: " + data)
        
        publish_future = publisher.publish(topic_path, data.encode("utf-8"))
        publish_future.add_done_callback(get_callback(publish_future, data))
        publish_futures.append(publish_future)

        rc.hset(user["user"], mapping=user)
        futures.wait(publish_futures, return_when=futures.ALL_COMPLETED)

    rc.close()
