from concurrent.futures import TimeoutError
from google.cloud import pubsub_v1
import json

# gcloud pubsub subscriptions create ${TOPIC_ID}-sub --topic=${TOPIC_ID}

subscriber = pubsub_v1.SubscriberClient()
subscription_path = subscriber.subscription_path("mague-tf", "maguec-dataflow-topic-sub")

def callback(message: pubsub_v1.subscriber.message.Message) -> None:
    try:
        js = json.loads(message.data.decode("utf-8"))
        print(js)
        message.ack()
    except:
        print("Could not decode:")
        print(f"{message}")
        message.ack()

streaming_pull_future = subscriber.subscribe(subscription_path, callback=callback)
print(f"Listening for messages on {subscription_path}..\n")

# Wrap subscriber in a 'with' block to automatically call close() when done.
with subscriber:
    try:
        # When `timeout` is not set, result() will block indefinitely,
        # unless an exception is encountered first.
        streaming_pull_future.result(timeout=None)
    except TimeoutError:
        streaming_pull_future.cancel()  # Trigger the shutdown.
        streaming_pull_future.result()  # Block until the shutdown is complete.
